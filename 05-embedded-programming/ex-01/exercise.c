#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "../mini-assert/mini-assert.h"

// Q4: Common type issues
void run_types_exercise(void) {
  puts("Types exercises");
  puts("------------------------------\n");
  unsigned int uint = 248;
  signed int sint = -1;
  if (sint > uint) {
    puts("T1: Why is this line printed? Why is -1 > 248?\n");
  }

  int vint = -1;
  printf("T2: What would this output be if we used %%u instead of %%d: %d? Explain why.\n\n", vint);

  double d;
  for (d = 10.0; d != 0.0; d -= 0.1) {
    //printf("%f\n",d);
    if (d < -1.0) {
      puts("T3: This loop should have terminated at 0.0. Why is this line still printed?\n");
      break;
    }
  }

  int initial = 0xFF;
  char down_cast = (char) initial;
  unsigned char new_char = (unsigned char) down_cast;
  unsigned int up_cast = new_char;
  int up_cast2 = (int) up_cast;
  printf("A: %d\n", initial);
  printf("B: %c\n", down_cast);
  printf("C: %u\n", up_cast);
  printf("D: %d\n", up_cast2);


  printf("T4: Why is initial != up_cast (%d != %d)? What's the difference between a signed integer downcast vs. upcast?\n", initial, up_cast);

  initial = 0x7F;
  down_cast = (char) initial;
  up_cast = (int) down_cast;
  printf("T4: initial == up_cast (%d != %d)? What is the difference to the previous situation?\n\n\n", initial, up_cast);
}

// Q5: Dube calculator
void run_get_cube(){
  double number;
	printf("Type in a number \n");
	scanf("%lf", &number);
  double cube = pow(number,3);
	printf("%.2lf^3 = %.2lf\n", number, cube);
}

// Q6: Bitwise operations
int flip_lsb(int value){
  return value ^ 0x00000001;
}
int flip_msb(int value) {
  return value ^ 0x80000000;
}
int clear_lsb(int value) {
  return ~(~value | 0x00000001);
}
int set_lsb_to_msb(int value) {
  return (value >> 31 & 0x00000001) | (value & 0xfffffffe);
}
int switch_portions(int value) {
  return (value >> 16 & 0x0000ffff) | (value << 16 & 0xffff0000);
}
int count_number_of_set_bits(int value) {
  int count = 0;
  for (int i = 0; i < 32; i++) {
    count += (value >> i & 0x0000001);
  }
  return count;
}
void run_bitwise_exercises(void) {
  puts("Bitwise exercise tests");
  puts("------------------------------");
  printf("\nflip_lsb\n");
  assert_equal_hex(flip_lsb(0x00000001), 0x00000000);
  assert_equal_hex(flip_lsb(0x00000000), 0x00000001);
  printf("\nflip_msb\n");
  assert_equal_hex(flip_msb(0x80000000), 0x00000000);
  assert_equal_hex(flip_msb(0x00000000), 0x80000000);
  assert_equal_hex(flip_msb(0x01234567), 0x81234567);
  assert_equal_hex(flip_msb(0x81234567), 0x01234567);
  printf("\nclear_lsb\n");
  assert_equal_hex(clear_lsb(0x00000001), 0x00000000);
  assert_equal_hex(clear_lsb(0x81234567), 0x81234566);
  printf("\nset_lsb_to_msb\n");
  assert_equal_hex(set_lsb_to_msb(0x80000000), 0x80000001);
  assert_equal_hex(set_lsb_to_msb(0x00000001), 0x00000000);
  assert_equal_hex(set_lsb_to_msb(0x01234561), 0x01234560);
  printf("\nswitch_portions\n");
  assert_equal_hex(switch_portions(0xABCD1234), 0x1234ABCD);
  assert_equal_hex(switch_portions(0x00000000), 0x00000000);
  assert_equal_hex(switch_portions(0x00010000), 0x00000001);
  printf("\ncount_number_of_set_bits\n");
  assert_equal_hex(count_number_of_set_bits(0xABCD0123), 14);
  assert_equal_hex(count_number_of_set_bits(0x00000000), 0);
  assert_equal_hex(count_number_of_set_bits(0x00000003), 2);
}

// Q7: Coordinates exercise
double distance_between_coordinates(int x1, int y1, int x2, int y2) {
  return pow(pow(x1-x2,2) + pow(y1-y2,2), 0.5);
}
void run_coordinates_exercise(void) {
  puts("Coordinates exercise tests");
  puts("------------------------------\n");
  assert_equal_delta(distance_between_coordinates(0, 0, 0, 0), 0.0);
  assert_equal_delta(distance_between_coordinates(0, 10, 20, 20), 22.36);
  assert_equal_delta(distance_between_coordinates(-10, -10, 20, 20), 42.42);
  assert_equal_delta(distance_between_coordinates(20, 20, -10, -10), 42.42);
  assert_equal_delta(distance_between_coordinates(50, 0, 80, 0), 30.0);
  puts("\n");
}

// Q8: Time conversion
void run_time_converter(void) {
  int minutes;
  int minutes_part;
  int hours;
  while (1) {
    printf("Type number of minutes \n");
    scanf("%d", &minutes);
    if (minutes == -1) {
      break;
    }
    minutes_part = minutes % 60;
    hours = (minutes - minutes_part) / 60;
    printf("%d minutes is %d:%02d \n", minutes, hours, minutes_part);
  }
}

// Q9: Echo program
void run_echo_exercise(void) {
  char *text ;
  while (1) {
    if (scanf("\n%m[^\n]", &text) == EOF) return;
    if (!strcmp("exit", text)) {
      break;
    }
    printf("%s\n", text);
    free(text);
  }
}

// Q10: Recursive program
void iterate(int current, int stop) {
  if (current > stop) return;
  printf("%d\n", current);
  iterate(current+1, stop);
  return;
}
void run_recursive_print() {
  int iter;
  scanf("%d", &iter);
  iterate(0, iter);
}

int main(void) {
  //run_types_exercise();
  //run_get_cube();
  //run_bitwise_exercises();
  //run_coordinates_exercise();
  //run_time_converter();
  //run_echo_exercise();
  //run_recursive_print();
  return 0;
}
