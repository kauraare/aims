#include <stdio.h>
#include <stdlib.h>

typedef long double intL;

intL *reserve_memory() {
  return (intL * ) malloc(100 * sizeof(intL));
}

void initialise_memory (intL *data) {
  int i;
  for (i=0; i<100; ++i) {
    data[i] = 0;
  }
}

void manipulate_data (intL *data) {
  data[0] = 1;
  data[1] = 1;
  int i;
  for (i=2; i<100; ++i) {
    data[i] = data[i-2] + data [i-1];
  }
}

void release_memory (intL *data) {
  free(data);
}

void run_malloc_free_exercise(void) {
  puts("Malloc/free exercises");
  puts("------------------------------\n");

  intL *pointer = reserve_memory();
  initialise_memory(pointer);
  manipulate_data(pointer);
  for (int i=0; i<100; ++i) {
    printf("%.0Lf\n", pointer[i]);
  }
  release_memory(pointer);

  puts("\n\n");
}
