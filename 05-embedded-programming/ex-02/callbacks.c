#include <stdio.h>

#define CALLBACK_STORAGE_SIZE 10

typedef void * callbackt;

void (*p[CALLBACK_STORAGE_SIZE]) () = {NULL};

void event_occurred(void) {
  for (int i=0; i<CALLBACK_STORAGE_SIZE; ++i) {
    if (p[i] != NULL) {
      (*p[i]) ();
    }
  }
}

void add_callback(callbackt callback) {
  for (int i=0; i<CALLBACK_STORAGE_SIZE; ++i) {
    if (p[i] == NULL) {
      p[i] = callback;;
      break;
    }
  }
}

void remove_callback(callbackt callback) {
  for (int i=0; i<CALLBACK_STORAGE_SIZE; ++i) {
    if (p[i] == callback) {
      p[i] = NULL;
      break;
    }
  }
}

void first(void) {
  puts("first");
}

void second(void) {
  puts("second");
}

void run_callbacks_exercise(void) {
  puts("Callback exercises");
  puts("------------------------------\n");
  add_callback(first);
  add_callback(second);
  event_occurred();
  remove_callback(second);
  event_occurred();
  puts("\n");
}
