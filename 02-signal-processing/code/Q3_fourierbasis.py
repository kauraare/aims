import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
from pygsp import graphs, filters, plotting


#G = graphs.Ring(N=60)
G = graphs.Minnesota()
#G = graphs.ErdosRenyi(N=30, p=0.2)
#G.set_coordinates(kind='spring')

G.compute_fourier_basis()
print(np.diag(G.U[:,0:4].T @ G.L @ G.U[:,0:4]))

fig, axes = plt.subplots(1, 2, figsize=(10, 4))
G.plot_signal(G.U[:, 3], vertex_size=20, ax=axes[0]) # the 3rd eigenvector as a signal on the graph
G.set_coordinates('line1D')
G.plot_signal(G.U[:, 0:4], ax=axes[1]) # the first three eigenvectors on the real line
fig.tight_layout()
plt.show()
