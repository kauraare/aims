import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
from pygsp import graphs, filters, plotting

G = graphs.Ring(N=60)
f = np.random.normal(size=G.N)
f_sorted = np.sort(f)

print(f @ G.L @ f.T)
print(f_sorted @ G.L @ f_sorted.T)

fig, axes = plt.subplots(1,2)
G.plot_signal(f,vertex_size=50, ax=axes[0])
G.plot_signal(f_sorted,vertex_size=50, ax=axes[1])
plt.show()
