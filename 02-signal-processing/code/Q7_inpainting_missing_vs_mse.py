import numpy as np
import scipy.linalg as linalg
import scipy.signal as signal
import matplotlib.pyplot as plt
from pygsp import graphs, filters, plotting
from multiprocessing import Pool
from itertools import repeat

def find_MSE(p, alpha):
    # Smooth signal
    G = graphs.Sensor(N)
    G.compute_fourier_basis()
    signal_0 = G.U[:, 0:n] @ np.random.randn(n, 1)
    signal_0 = signal_0 - np.min(signal_0)
    signal_0 = signal_0 / np.max(signal_0)

    # Partially observed signal
    missing = int(N*p)
    mask = np.expand_dims(np.random.permutation(np.concatenate([np.zeros(missing), np.ones(N-missing)])),1)
    Mask = np.diag(np.squeeze(mask))
    signal_n = signal_0 * mask

    # reconstruct and scale
    signal_r = linalg.solve(alpha * (G.L + G.L.T) + 2 * Mask**2, 2 * Mask @ signal_n )
    signal_r = signal_r - np.min(signal_r)
    signal_r = signal_r / np.max(signal_r)

    # return mse
    return np.linalg.norm(signal_r - signal_0) / G.N**(1/2)

N = 100 # number of vertices
n = 4 # number of components to build smooth signal
p = np.linspace(0,0.99,1000)

win=signal.windows.gaussian(201,100)
win=win/np.sum(win)

for alpha in [0.1, 1, 10]:
    with Pool() as pool:
        mse = pool.starmap(find_MSE, zip(p, repeat(alpha)))
    mse_lowpass = signal.convolve(mse, win)
    plt.plot(p[100:-100], mse_lowpass[200:-200], '-', label=r'$\alpha = ' + str(alpha) + '$')
plt.xlabel('Proportion of signal missing')
plt.ylabel('Mean square error of reconstructed signal')
plt.legend()
plt.tight_layout()
plt.savefig('figures/missing-vs-mse.pdf')
