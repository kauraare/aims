import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
from pygsp import graphs, filters, plotting

#G = graphs.Minnesota()
G = graphs.ErdosRenyi(N=30, p=0.2)
G.set_coordinates(kind='spring')

D = np.diag(G.L.diagonal() ** (-1/2))
L_norm = D @ G.L @ D

fig, axes = plt.subplots(2, 3)
axes[0,0].spy(G.W, markersize=2)
G.plot(ax=axes[1,0])
axes[0,1].spy(G.L, markersize=5)
axes[1,1].hist(G.L.data, bins=100, log=True)
axes[1,2].hist(L_norm.reshape(-1), bins=100, log=True)
axes[0,2].imshow(L_norm)
plt.show()
