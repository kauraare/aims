import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
from pygsp import graphs, filters, plotting
import imageio


n = 128 # Resolution
N = n**2 # pixels
alpha = 0.1 # tradeoff paramter

# Import
im = imageio.imread('code/cameraman' + str(n) + '.png')
signal_0 = im.reshape(-1)
signal_0 = np.expand_dims(signal_0, 1)

i=0
fig, axes = plt.subplots(1,3, figsize=(15, 15))
for p in [0.3, 0.6, 0.9]: # fraction of missing pixels
    missing = int(N*p) # numer of missing pixels

    # Partially observed signal
    mask = np.random.permutation(np.concatenate([np.zeros(missing), np.ones(N-missing)]))
    mask = np.expand_dims(mask,0)
    Mask = sp.sparse.diags(mask, [0])
    signal_n = signal_0 * mask.T

    # reconstructed signal
    G = graphs.Grid2d(n,n)
    signal_r = sp.sparse.linalg.spsolve(alpha * (G.L + G.L.T) + 2 * Mask**2, 2 * Mask @ signal_n )

    plt.subplot(3,3,1+i)
    plt.imshow(signal_0.reshape(n,n), cmap='gray')
    plt.subplot(3,3,2+i)
    plt.imshow(signal_n.reshape(n,n), cmap='gray')
    plt.subplot(3,3,3+i)
    plt.imshow(signal_r.reshape(n,n), cmap='gray')
    i=i+3
plt.tight_layout()
plt.savefig('figures/cameraman-reconstruction.pdf')
