import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
from pygsp import graphs, filters, plotting

N = 100 # number of vertices
n = 4 # number of components to build smooth signal
p = 0.9 # fraction of missing signal
alpha = 3

# Smooth signal
G = graphs.Sensor(N)
G.compute_fourier_basis()
signal_0 = G.U[:, 0:n] @ np.random.randn(n, 1)
signal_0 = signal_0 - np.min(signal_0)
signal_0 = signal_0 / np.max(signal_0)

# Partially observed signal
missing = int(N*p)
mask = np.expand_dims(np.random.permutation(np.concatenate([np.zeros(missing), np.ones(N-missing)])),1)
Mask = np.diag(np.squeeze(mask))
signal_n = signal_0 * mask

# reconstructed signal
signal_r = sp.linalg.solve(alpha * (G.L + G.L.T) + 2 * Mask**2, 2 * Mask @ signal_n )

# reconstructed and scaled
signal_rs = signal_r - np.min(signal_r)
signal_rs = signal_rs / np.max(signal_rs)

print(f'MSE partially observed:       {np.linalg.norm(signal_n - signal_0) / G.N**(1/2):12.4f}')
print(f'MSE reconstructed:            {np.linalg.norm(signal_r - signal_0) / G.N**(1/2):12.4f}')
print(f'MSE reconstructed and scaled: {np.linalg.norm(signal_rs- signal_0) / G.N**(1/2):12.4f}')

fig, axes = plt.subplots(2, 2, figsize=(9,9))
G.plot_signal(signal_0, ax=axes[0,0])
G.plot_signal(signal_n, ax=axes[0,1])
G.plot_signal(signal_r, ax=axes[1,0])
G.plot_signal(signal_rs, ax=axes[1,1])

plt.tight_layout()
plt.savefig('figures/inpainting.pdf')
