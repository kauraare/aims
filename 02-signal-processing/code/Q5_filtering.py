import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
from pygsp import graphs, filters, plotting
import copy

communities = [80, 60, 40]
G = graphs.Community(N=np.sum(communities), Nc=len(communities), comm_sizes=communities)

f = np.random.normal(size=G.N)
fp = np.zeros(G.N)
fp[:communities[0]] = -1 * np.ones(communities[0])
fp[-communities[-1]:] = 1 * np.ones(communities[-1])


G.compute_fourier_basis()
f_hat = G.gft(f)

fb_hat = copy.deepcopy(f_hat)
fb_hat[10:] = 0 # this corresponds to an ideal brick-wall low-pass filtering
fb = G.igft(fb_hat)

tau = 40
delta = 10
g = filters.Heat(G, tau) # the heat kernel defined via the graph spectral domain

s = g.localize(delta) # the heat kernel localised at a particular node in the vertex domain
s_hat = G.gft(s)

kron = np.zeros(G.N)
kron[delta] = 1
fh = g.filter(kron)
fh_hat = G.gft(fh)


fig, axes = plt.subplots(2, 4, figsize=(32, 18))
G.plot_signal(f, vertex_size=30, ax=axes[0,0])
axes[1,0].plot(G.e, np.abs(f_hat), '.-')
G.plot_signal(fb, vertex_size=30, ax=axes[0,1])
axes[1,1].plot(G.e, np.abs(fb_hat), '.-')
G.plot_signal(s, vertex_size=30, ax=axes[0,2])
axes[1,2].plot(G.e, np.abs(s_hat), '.-')
#g.plot(ax=axes[1,2])
G.plot_signal(fh, vertex_size=30, ax=axes[0,3])
axes[1,3].plot(G.e, np.abs(fh_hat), '.-')

plt.show()
