import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
from pygsp import graphs, filters, plotting

communities = [80, 60, 40]
G = graphs.Community(N=np.sum(communities), Nc=len(communities), comm_sizes=communities)

f = np.random.normal(size=G.N)
fp = np.zeros(G.N)
fp[:communities[0]] = -1 * np.ones(communities[0])
fp[-communities[-1]:] = 1 * np.ones(communities[-1])


G.compute_fourier_basis()
f_hat = G.gft(f)
fp_hat = G.gft(fp)

print(f.T @ G.L @ f / (np.linalg.norm(f) ** 2))
print(fp.T @ G.L @ fp / (np.linalg.norm(fp) ** 2))

fig, axes = plt.subplots(2, 2, figsize=(32, 18))
G.plot_signal(f, vertex_size=30, ax=axes[0,0])
axes[0,1].plot(G.e, np.abs(f_hat), '.-')
G.plot_signal(fp, vertex_size=30, ax=axes[1,0])
axes[1,1].plot(G.e, np.abs(fp_hat), '.-')
plt.show()
