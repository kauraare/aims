import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
from pygsp import graphs, filters, plotting

N = 50 # number of vertices
n = 4 # number of components to build smooth signal
noise = 1 # noise amplitude
alpha = 3

# Smooth signal
G = graphs.Sensor(N)
G.compute_fourier_basis()
signal_0 = G.U[:, 0:n] @ np.random.randn(n, 1)
signal_0 = signal_0 / np.linalg.norm(signal_0)

# Noisy signal
signal_n = signal_0 + noise * np.random.randn(G.N, 1)

# Filtered signal
signal_f = sp.linalg.solve(alpha * (G.L + G.L.T) + 2 * np.eye(G.N), 2 * signal_n )

print(np.linalg.norm(signal_n - signal_0) / G.N**(1/2))
print(np.linalg.norm(signal_f - signal_0) / G.N**(1/2))


fig, axes = plt.subplots(1,3, figsize=(32, 18))
G.plot_signal(signal_0, ax=axes[0])
G.plot_signal(signal_n, ax=axes[1])
G.plot_signal(signal_f, ax=axes[2])
plt.show()
