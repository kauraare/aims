import numpy as np
from sklearn.cluster import KMeans
from sklearn.datasets import load_breast_cancer
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

dataset = load_breast_cancer()
N = 400 # points for training
D = dataset.data.shape[1] # number of dimensions

data = dataset.data # - dataset.data.mean(0) # make zero mean

X_train = data[:N]
Y_train = dataset.target[:N]
X_test = data[N:]
Y_test = dataset.target[N:]

xtx = X_train.T @ X_train

nl=3 # number of principal eigenvectors to be found
V = np.zeros([nl,D])
for i in range(nl):
    v = np.random.randn(D)
    while True:
        v_next = xtx @ v
        v_next -= (V[:i] * np.expand_dims(V[:i] @ v_next, 1)).sum(0)
        v_next /= np.linalg.norm(v_next)
        if np.linalg.norm(v - v_next) < 1e-12:
            break
        v = v_next
    V[i] = v

X_train_pca = X_train @ V.T
X_test_pca = X_test @ V.T

km = KMeans(n_clusters=2).fit(X_train_pca)
km.labels_ = Y_train
Y_predict = km.predict(X_test_pca)

if np.linalg.norm(Y_predict - Y_test, 1) > (dataset.data.shape[0]-N)/2:
    Y_predict = 1 - Y_predict

XX=np.concatenate([X_train_pca,X_test_pca])
YY=np.concatenate([Y_train, Y_predict+2])

print(Y_predict - Y_test)
print(np.linalg.norm(Y_predict - Y_test, 1))

fig = plt.figure()
ax = Axes3D(fig)
p = ax.scatter(XX[:,0], XX[:,1], XX[:,2], c=YY, cmap='jet', s=50)
plt.show()
