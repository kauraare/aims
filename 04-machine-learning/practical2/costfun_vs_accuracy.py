import numpy as np
from sklearn.datasets import fetch_20newsgroups
from sklearn.feature_extraction.text import TfidfVectorizer
from multiprocessing import Pool
import matplotlib.pyplot as plt
import datetime

import myfunctions

# Load data
print('Loading and vectorising')
cats = [ 'sci.med', 'misc.forsale', 'soc.religion.christian']
newsgroups_train = fetch_20newsgroups(subset='train', categories=cats)
newsgroups_test = fetch_20newsgroups(subset='test', categories=cats)
vectorizer = TfidfVectorizer()
vectors = vectorizer.fit_transform(newsgroups_train.data)

print('Doing PCA')
kerneltype = 'poly2'
K = myfunctions.kernel(vectors,kerneltype)
w, u = np.linalg.eigh(K)

d = 30
data = ((K @ u[:,-d:]) / w[-d:])
def cost_and_accuracy(foo):
    (labels, cost) = myfunctions.kmeans(data, len(cats), 'random', newsgroups_train, getCost=True)
    accuracy = myfunctions.classification_accuracy(newsgroups_train.target, labels, len(cats))
    return (cost, accuracy)

print('Running k-means')
N = 10000
with Pool() as pool:
    result = pool.map(cost_and_accuracy, range(N))
result = np.array(result)

plt.figure(figsize=(12,8))
plt.plot(result[:,0],result[:,1],'o',markersize=0.5)
plt.xlabel('Converged cost function value')
plt.ylabel('Classification accuracy')
plt.savefig('../figures/costfunvalue_vs_accuracy-' + kerneltype + '-' + str(N) + 'points-' + datetime.datetime.now().strftime("%Y%m%d-%H%M%S") +'.pdf')
