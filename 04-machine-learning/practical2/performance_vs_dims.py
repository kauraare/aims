import numpy as np
from sklearn.datasets import fetch_20newsgroups
from sklearn.feature_extraction.text import TfidfVectorizer
from multiprocessing import Pool
import matplotlib.pyplot as plt
import datetime

import myfunctions

def all_methods_performance(d):
    print('D=' + str(d))
    data = ((K @ u[:,-d:]) / w[-d:])
    performance = np.ones([len(methods)])
    for idx_method,method in enumerate(methods):
        labels = myfunctions.kmeans(data, len(cats), method, newsgroups_train)
        performance[idx_method] = myfunctions.classification_accuracy(newsgroups_train.target, labels, len(cats))
    return performance

# Load data
print('Loading and vectorising')
cats = [ 'sci.med', 'misc.forsale', 'soc.religion.christian']
newsgroups_train = fetch_20newsgroups(subset='train', categories=cats)
newsgroups_test = fetch_20newsgroups(subset='test', categories=cats)
vectorizer = TfidfVectorizer()
vectors = vectorizer.fit_transform(newsgroups_train.data)

print('Doing PCA')
kerneltype = 'RBF'
K = myfunctions.kernel(vectors,kerneltype)
w, u = np.linalg.eigh(K)

D = np.linspace(1,201,101).astype(int)
methods = ['sklearn','random','random_100','random_10000','extremes','best']

with Pool() as pool:
    result = pool.map(all_methods_performance, D)
performance = np.array(result)


plt.figure(figsize=(12,8))
plt.plot(D,performance)
plt.legend(methods)
plt.xlabel('Number of Dimensions')
plt.ylabel('Classification accuracy')
plt.savefig('../figures/classification_accuracy-' + kerneltype + '-' + datetime.datetime.now().strftime("%Y%m%d-%H%M%S") +'.pdf')
