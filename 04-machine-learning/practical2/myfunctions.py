import numpy as np
import scipy as sp
from itertools import permutations
import re
import warnings
from sklearn.cluster import KMeans

# Polynomial kernel order 2 and normalise
def kernel(vectors,kerneltype):
    products = vectors @ vectors.T
    if kerneltype is 'poly2':
        K = (products.toarray() + 1) ** 2
    elif kerneltype is 'RBF':
        sigma = 1
        norm_sq = sp.sparse.linalg.norm(vectors, axis=1)
        dist_sq = norm_sq + np.expand_dims(norm_sq,1) - 2 * products
        K = np.array(np.exp(-dist_sq / (2 * sigma**2)))
        dist_sq.sort()
    else:
        return -1
    K = K - K.mean(0) - np.expand_dims(K.mean(1),1) + K.mean()
    return K

def kmeans(data, Nc, method, newsgroups_train, getCost=False):
    ### sklearn kmeans algorithm
    if method is 'sklearn':
        return KMeans(n_clusters=Nc).fit(data).labels_

    ### Random point initilaisition
    if method is 'random':
        np.random.seed()
        mu = data[np.random.choice(len(data), Nc)]

    ### Random best of n
    if re.match(r"random_[0-9]+",method):
        best = 0
        for i in range(int(re.match(r"random_([0-9]+)",'random_100').group(1))):
            current_idx = kmeans(data, Nc, 'random', newsgroups_train)
            current = classification_accuracy(newsgroups_train.target, current_idx, Nc)
            if current > best:
                best_idx = current_idx
                best = current
        return best_idx

    ### choose the points farthest to each other
    if method is 'extremes':
        norm_sq = np.linalg.norm(data, axis=1) ** 2
        dist = (norm_sq + np.expand_dims(norm_sq, 1)) - 2 * data @ data.T
        dist = np.triu(dist,1)

        mask=np.ones([Nc+1,Nc+1,Nc+1],dtype=bool)
        mask[np.arange(Nc+1), :, np.arange(Nc+1)] = False
        mask[np.arange(Nc+1), np.arange(Nc+1), :] = False

        selection = np.arange(Nc)
        dist_sel = dist[:Nc,:Nc]
        for i in np.arange(Nc,len(data)):
            selection = np.concatenate([selection, [i]])
            dist_sel = np.hstack([np.vstack([dist_sel,np.zeros(Nc)]), dist[selection,i:i+1]])
            worst = np.expand_dims(dist_sel, 0).repeat(Nc+1,axis=0)[mask].reshape(Nc+1,Nc,Nc).sum(1).sum(1).argmax()
            selection = np.delete(selection, worst)
            dist_sel = np.delete(dist_sel, worst, axis=0)
            dist_sel = np.delete(dist_sel, worst, axis=1)
        #print(dist_sel)
        mu = data[selection]

    ### Choose means as centres of actual clusters (cheating)
    if method is 'best':
        mask=np.empty([len(data),1,Nc])
        mask[:] = np.nan
        mask[np.arange(len(data)),:,newsgroups_train.target] = 1
        mu = np.nanmean(mask * np.expand_dims(data,2),0).T

    ### Run k-means
    while True:
        dist = np.linalg.norm((data - np.expand_dims(mu,1)), axis=2)
        min_idx = dist.argmin(axis=0)
        cost = dist[min_idx, np.arange(len(data))].sum()
        #print(cost)
        mask = np.ones(dist.shape).T
        mask[np.arange(Nc) != np.expand_dims(min_idx,1)] = np.nan
        with warnings.catch_warnings():
            warnings.simplefilter("ignore", category=RuntimeWarning)
            mu_new = np.nanmean(np.expand_dims(mask,2) * np.expand_dims(data,1), 0)
        if np.any(np.any(np.isnan(mu_new),axis=1)):
            empties = np.nonzero(np.any(np.isnan(mu_new),axis=1))[0]
            mu_new[empties] = data[np.random.choice(len(data),len(empties))]
        if  np.any(np.linalg.norm(mu - mu_new, axis=1) < 1e-12) :
            break
        mu = mu_new

    if getCost==True:
        return (min_idx, cost)
    else:
        return min_idx

def classification_accuracy(true_labels, calculated_labels, Nc):
    a = np.stack([calculated_labels,true_labels]).T
    uniq = np.stack([np.arange(Nc).repeat(Nc),np.expand_dims(np.arange(Nc),1).repeat(Nc,1).T.reshape(-1)]).T
    labels_matrix = (np.linalg.norm((np.expand_dims(a,1)-uniq),axis=2) == 0).sum(0).reshape([Nc,Nc])
    return labels_matrix[np.array(list(set(permutations(np.arange(Nc))))), range(Nc)].sum(1).max() / labels_matrix.sum()
