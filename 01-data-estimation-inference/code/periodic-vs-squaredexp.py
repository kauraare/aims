import pandas as pd
import numpy as np
import scipy.optimize as opt
import matplotlib.pyplot as plt
import os
import gp

# set figure name
cwd = os.path.dirname(os.path.realpath(__file__))
figurename = cwd + '/../figures/' + os.path.basename(__file__)[:-3] + '.pdf'

# import data to variables
data = pd.read_csv(cwd + '/sotonmet.txt')
for i in [0, 2]:
    t = pd.to_numeric(pd.to_datetime(data.iloc[:, i])).values/1e9/60/60
    t = t - t[0]
    data.iloc[:, i] = t
y_true = data.values[:, 10]
y_all = data.values[:, 5]
t_all = data.values[:, 0]
t_train = t_all[~np.isnan(y_all)]
y_train = y_all[~np.isnan(y_all)]

# Define covariance functions and initial hyperparameters
cov = gp.squarexp
hyp_0 = [2.4, 2.2]
sigma_N = 0.1
loghyp_0 = np.log(hyp_0) # take log for unconstrained optimisation

# Optimise hyperparameters
opt_res = opt.minimize(gp.negloglik, loghyp_0, args=(cov,sigma_N, t_train, y_train), tol=1e-1)
print(f'Best:\nloglik: {-opt_res.fun:12.4f}   hyp:[', *[f'{x:7.4f},' for x in np.exp(opt_res.x)],']')
mu_y, sigma_y = gp.predict(np.exp(opt_res.x), cov, sigma_N, t_train, y_train, t_all)

# plot
plt.figure(figsize=(12,5))
plt.rc('text', usetex=True)
plt.rc('font', family='serif')
plt.subplot(1,2,1)
plt.fill_between(t_all, mu_y - sigma_y, mu_y + sigma_y, color = (0, 0, 1, 0.2), label="GP - $1\sigma$ range")
plt.plot(t_all, mu_y, label="GP - mean")
plt.plot(t_all, y_all, label="Training data")
plt.plot(t_all, y_true, label="True data")
plt.title('Squared exponential kernel')
plt.xlabel('Elapsed time (h)')
plt.ylabel('Tide height (m)')
plt.legend()

# Define covariance functions and initial hyperparameters
cov = gp.periodic
hyp_0 = [5, 12.4, 10]
sigma_N = 0.15
loghyp_0 = np.log(hyp_0) # take log for unconstrained optimisation

# Optimise hyperparameters
opt_res = opt.minimize(gp.negloglik, loghyp_0, args=(cov,sigma_N, t_train, y_train), tol=1e-1)
print(f'Best:\nloglik: {-opt_res.fun:12.4f}   hyp:[', *[f'{x:7.4f},' for x in np.exp(opt_res.x)],']')
mu_y, sigma_y = gp.predict(np.exp(opt_res.x), cov, sigma_N, t_train, y_train, t_all)

# plot
plt.subplot(1,2,2)
plt.fill_between(t_all, mu_y - sigma_y, mu_y + sigma_y, color = (0, 0, 1, 0.2), label="GP - $1\sigma$ range")
plt.plot(t_all, mu_y, label="GP - mean")
plt.plot(t_all, y_all, label="Training data")
plt.plot(t_all, y_true, label="True data")
plt.title('Periodic kernel')
plt.xlabel('Elapsed time (h)')
plt.ylabel('Tide height (m)')
plt.legend()

plt.tight_layout()
plt.savefig(figurename)
