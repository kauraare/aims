import numpy as np
import scipy.linalg as sclinalg

class Kernel:
    def __init__(self, n_hyp, func):
        self.func = func
        self.n_hyp = n_hyp
    def __call__(self, hyp, diff):
        return self.func(hyp, diff)
    def __add__(self, other):
        return self.__class__(self.n_hyp + other.n_hyp, lambda hyp, diff: self.func(hyp[:self.n_hyp], diff) + other.func(hyp[self.n_hyp:], diff))
    def __mul__(self, other):
        return self.__class__(self.n_hyp + other.n_hyp, lambda hyp, diff: self.func(hyp[:self.n_hyp], diff) * other.func(hyp[self.n_hyp:], diff))

# define base covariance function
squarexp = Kernel(2, lambda hyp, diff: hyp[0]**2 * np.exp(-diff**2/(2*hyp[1]**2)))
periodic = Kernel(3, lambda hyp, diff: hyp[0]**2 * np.exp(-2*np.sin(np.pi*diff/hyp[1])**2/hyp[2]))
matern32 = Kernel(2, lambda hyp, diff: hyp[0]**2 * np.exp(-np.sqrt(3)*diff/hyp[1]) * (1 + np.sqrt(3)*diff/hyp[1]))
matern52 = Kernel(2, lambda hyp, diff: hyp[0]**2 * np.exp(-np.sqrt(5)*diff/hyp[1]) * (1 + np.sqrt(5)*diff/hyp[1] + 5/3 * np.power(diff/hyp[1], 2)))

# Calculate negative return likelihood. Print each iteration
def negloglik(loghyp, cov, sigma_N, t_train, y_train):
    hyp = np.exp(loghyp)
    K = cov(hyp, np.abs(t_train - np.expand_dims(t_train, 1))) + sigma_N**2 * np.eye(t_train.size)
    L = sclinalg.cho_factor(K,lower=True)
    alpha = sclinalg.cho_solve(L, y_train)
    loglik = -1/2 * y_train.T @ alpha - np.log(np.diag(L[0])).sum(axis=0)
    print(f'loglik: {loglik:12.4f}   hyp:[', *[f'{x:7.4f},' for x in hyp],']')
    return -loglik

# Predict values given covariance function with hyperparameter values, training data
def predict(hyp, cov, sigma_N, t_train, y_train, t_predict):
    K = cov(hyp, np.abs(t_train - np.expand_dims(t_train, 1))) + sigma_N**2 * np.eye(t_train.size)
    k_predict = cov(hyp, np.abs(t_train - np.expand_dims(t_predict,  1))).T
    L = sclinalg.cho_factor(K,lower=True)
    mu_y = k_predict.T @ sclinalg.cho_solve(L, y_train)
    v = np.linalg.solve(np.tril(L[0]),k_predict)
    sigma_y = (-(v * v).sum(axis=0) + sigma_N**2 + cov(hyp,0))**0.5
    return mu_y, sigma_y
