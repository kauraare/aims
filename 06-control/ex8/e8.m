%%

hold on
A = [-0.003, 0.039, 0, -0.322;
    -0.065, -0.319, 7.747, 0;
    0.02, -0.101, -0.429, 0;
    0, 0, 1, 0]

B = [0.01, 1.00; -0.18, -0.04; -1.16, 0.6; 0, 0]

C = [0, 1, 0, 0; 0, 0, 1, 0]

R = eye(2)
Q = eye(4)

%%

%%%%%%%%
[x,l,k] =care(A, B, Q)



%%%%%%%%
cvx_begin sdp
    variable Y(2, 4)
    variable S(4, 4) symmetric
    maximize(trace(S))
    subject to
    [A*S+S*A'+Y'*B'+B*Y, S*Q^.5, Y'*R^.5;
        Q^.5*S, -eye(4), zeros(4, 2);
        R^.5*Y, zeros(2, 4), -eye(2)] <= 0
        S >= 0
cvx_end


K = -Y*(S')^-1
plot(eig(A-B*K), 'o')
K == k

A1 = A
A1(2,3) = A1(2,3)*1.1

A2 = A
A2(2,3) = A2(2,3)*0.9

cvx_begin sdp
    variable Y(2, 4)
    variable S(4, 4) symmetric
    maximize(trace(S))
    subject to
    [A1*S+S*A1'+Y'*B'+B*Y, S*Q^.5, Y'*R^.5;
        Q^.5*S, -eye(4), zeros(4, 2);
        R^.5*Y, zeros(2, 4), -eye(2)] <= 0;
        S >= 0
    [A2*S+S*A2'+Y'*B'+B*Y, S*Q^.5, Y'*R^.5;
        Q^.5*S, -eye(4), zeros(4, 2);
        R^.5*Y, zeros(2, 4), -eye(2)] <= 0
   
cvx_end

K = -Y*(S')^-1
plot(eig(A1-B*K), 'o')
plot(eig(A2-B*K), 'o')
plot(l, 'o')
    


%%
%cvx_solver mosek
epsilon = 0;
r = 2.5*2
th = pi/6
L1 = [[-r,0];[0,-r]]
M1 = [[0,1];[-1,0]]
L2 = [[0,0];[0,0]]
M2 = [[sin(th),cos(th)];[-cos(th),sin(th)]]

cvx_begin sdp
    variable Y(2,4)
    variable S(4,4) symmetric
    maximise(trace(S))
    subject to
    kron(L1,S) + kron(M1, A*S) - kron(M1, -B*Y) + kron(M1', S*A') - kron(M1',-Y'*B') <= -eps*eye(8)
    kron(L2,S) + kron(M2, A*S) - kron(M2, -B*Y) + kron(M2', S*A') - kron(M2',-Y'*B') <= -eps*eye(8)
    
   
    [A*S+S*A'+Y'*B'+B*Y, S*Q^.5, Y'*R^.5;
        Q^.5*S, -eye(4), zeros(4, 2);
        R^.5*Y, zeros(2, 4), -eye(2)] <= 0
    S >= 0
cvx_end

K = (-Y)*inv(S)
plot(eig(A-B*K), 'o')
hold off