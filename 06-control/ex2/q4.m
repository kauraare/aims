pkg load control
clear;close all;clc;

A=  [
    -0.003,  0.039,  0     , -0.322;
    -0.065, -0.319,  7.747 ,  0    ;
     0.020, -0.101, -0.429 ,  0    ;
     0    ,  0    ,  1     ,  0    ;
];
B = [
     0.01,  1.00;
    -0.18, -0.04;
    -1.16,  0.60;
     0   ,  0   ;
];

B = [B, A(:,2)]

C = eye(4);
D = zeros(4,0);

sys = ss(A,B,C,D);

x0 = [0, 0, 0, 0.1];

t1 = linspace(0, 3000, 10000);
u1 = zeros(length(t1), 3);

t2 = linspace(0, 50, 10000);
u2 = zeros(length(t2), 3);
u2(:,3) = 3.5 * (t2<5);

y1 = lsim(sys, u1,  t1, x0);
y2 = lsim(sys, u2,  t2, x0);

damp(sys)
abs(eig(A))

subplot(3,1,1)
plot(eig(A),'x','Markersize',12)

subplot(3,1,2)
plot(t1,y1)
legend('\Delta u','\Delta w','\Delta q','\Delta \theta','Location','northoutside','Orientation','Horizontal')

subplot(3,1,3)
plot(t2,y2)
legend('\Delta u','\Delta w','\Delta q','\Delta \theta','Location','northoutside','Orientation','Horizontal')
