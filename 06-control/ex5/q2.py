import control
import numpy as np
import cvxpy as cvx

states=10
outputs=4
inputs=3
sys = control.drss(states=states,outputs=outputs,inputs=inputs)
A = np.array(sys.A)

A = A - (0.1 + np.max(np.linalg.eig(A)[0]) ) * np.eye(states)

B = np.array(sys.B)
# Q = np.array(sys.C.T @ sys.C)
Q = np.eye(states)

R = 10000 * np.eye(inputs)
P = cvx.Variable((states, states), PSD=True)


# -(Q + A.T @ P + P @ A),   P @ B
# (P @ B).T             ,   R
schur = cvx.vstack([cvx.hstack([-(Q + A.T @ P + P @ A), P @ B]),cvx.hstack([(P @ B).T , R])])
constraints = [schur >> 0 ]
obj = cvx.Minimize(cvx.trace(P))

prob = cvx.Problem(obj, constraints)
prob.solve()  # Returns the optimal value.

print("status:", prob.status)
print("optimal value", prob.value)
print("optimal var", P.value)
