import numpy as np
import sympy as sp
import cvxpy as cvx
import matplotlib.pyplot as plt

A = np.array([[[8,6],[6,8]],[[6,1],[1,6]],[[4,-3],[-3,4]]])
B = np.array([[[5,10]],[[-10,-30]],[[2,-10]]])
C = np.array([[[-100]],[[25]],[[2]]])

# A = np.array([[[1,0],[0,1]],[[2,0],[0,2]],[[3,0],[0,3]]])
# B = np.array([[[2,2]],[[0,0]],[[2,-2]]])
# C = np.array([[[-4]],[[-2]],[[-1]]])

ellipsoids = np.concatenate((np.concatenate((A,B.swapaxes(1,2)),axis=2),np.concatenate((B,C),axis=2)),axis=1)

mu = cvx.Variable((2,1))
gamma = cvx.Variable((1,1))
rho = cvx.Variable((1,1),nonneg=True)
tau = cvx.Variable((3,1),nonneg=True)

ball = cvx.vstack([cvx.hstack([np.eye(2),-mu]),cvx.hstack([-mu.T, gamma])])
ball_c = cvx.vstack([cvx.hstack([np.eye(2),mu]),cvx.hstack([mu.T, gamma+rho])])
constraints = [t*e >> ball for t, e in zip(tau, ellipsoids)]
constraints.append(ball_c >> 0)

obj = cvx.Minimize(rho)
prob = cvx.Problem(obj, constraints)
prob.solve()  # Returns the optimal value.

print("status:", prob.status)
print("optimal value", prob.value)
print("optimal var\n", ball.value)

ellipsoids=np.concatenate([ellipsoids,np.expand_dims(ball.value,0)],axis=0)

x=sp.Symbol('x')
y=sp.Symbol('y')

expr = x**2*ellipsoids[:,0,0] + 2*x*y*ellipsoids[:,0,1] + y**2*ellipsoids[:,1,1] + 2*x*ellipsoids[:,0,2] + 2*y*ellipsoids[:,1,2] + ellipsoids[:,2,2]

for e in expr:
    f = sp.solve(e, [y])
    xpts = np.linspace(-20,20,1000)
    ypts = sp.lambdify(x, f, 'numpy')(xpts)

    ypts = np.hstack([ypts[0],np.flip(ypts[1])])
    xpts = np.hstack([xpts,np.flip(xpts)])
    xpts = xpts[~np.isnan(ypts)]
    ypts = ypts[~np.isnan(ypts)]
    xpts = np.append(xpts,xpts[0])
    ypts = np.append(ypts,ypts[0])

    plt.plot(xpts,ypts)
plt.show()
