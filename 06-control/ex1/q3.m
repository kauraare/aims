pkg load signal;
clear; close all;
const.c = 1.5;
const.k = 2;
const.m = 1;

function dydt = springmassdamper(t,y,const)
  u = 5*square(2*t);
  dydt = [
    y(2);
    (-const.c*y(2) - const.k*y(1) + u) / const.m
   ];
end

tspan = [0 100];
initial_conditions = [0; 0];
opts = odeset('RelTol',1e-6,'AbsTol',1e-10);
[t,y] = ode23(@(t,y) springmassdamper(t,y,const), tspan, initial_conditions, opts);

subplot(2,1,1)
hold on
plot(t, y(:,1));
plot(t, y(:,2));
xlabel('time')
ylabel('q')
legend('q''','q''''')

subplot(2,1,2);
plot(y(:,2),y(:,1));
xlabel('q''')
ylabel('q''''')
