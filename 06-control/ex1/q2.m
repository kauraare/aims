clear; close all;
k.a=5;
k.b=5;
k.c=4;
k.d=8;

function dydt = hl(t,y,k)
  dydt = [k.b*y(1) - k.a*y(1)*y(2);
   k.c*y(1)*y(2) - k.d*y(2)];
end

tspan = [0 5];
initial_conditions = [5; 5];
%initial_conditions = [2.0001; 1];
opts = odeset('RelTol',1e-6,'AbsTol',1e-10);
[t,y] = ode23(@(t,y) hl(t,y,k), tspan, initial_conditions, opts);

subplot(2,1,1)
hold on
plot(t, y(:,1));
plot(t, y(:,2));
xlabel('time')
ylabel('population')
legend('hare','lynx')

subplot(2,1,2);
plot(y(:,1),y(:,2));
xlabel('hare')
ylabel('lynx')
