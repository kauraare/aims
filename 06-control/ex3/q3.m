clear;close all;
a=5;
b=5;
c=4;
d=8;

N=50;
h=linspace(-2,6,N);
l=linspace(-2,6,N);

[h,l] = meshgrid(h,l);

% true model
hdot = b*h - a*l.*h;
ldot = c*l.*h - d*l;

% linearised
J = [0, -a*d/c; c*b/a, 0];
eq = [d/c; b/a];
hl=[reshape(h,1,[]);reshape(l,1,[])];
hdotldot_lin = J * (hl - eq);
hdot_lin = reshape(hdotldot_lin(1,:),N,N);
ldot_lin = reshape(hdotldot_lin(2,:),N,N);

% error
hdot_err = hdot - hdot_lin;
ldot_err = ldot - ldot_lin;

% plot
subplot(2,2,1)
quiver(h,l,hdot,ldot,scale=4)
axis([h(1,1),h(end,end),l(1,1),l(end,end)])
xlabel('hare')
ylabel('lynx')


subplot(2,2,2)
quiver(h,l,hdot_lin,ldot_lin,scale=2)
axis([h(1,1),h(end,end),l(1,1),l(end,end)])
xlabel('hare')
ylabel('lynx')

subplot(2,2,3)
quiver(h,l,hdot_err,ldot_err,scale=2)
axis([h(1,1),h(end,end),l(1,1),l(end,end)])
xlabel('hare')
ylabel('lynx')
