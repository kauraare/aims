clear;close all;

M=4;
m=1;
l=1;
g=10;

A=[
  0, 0          , 1, 0;
  0, 0          , 0, 1;
  0, m*g/M      , 0, 0;
  0, (M+m)*g/M/l, 0, 0;
];

B=[0; 0; 1/M; 1/M/l];


C_P=[1,0,0,0];
C_theta=[0,1,0,0];

controllability = [B, A*B, A^2*B, A^3*B]
rank(controllability)

observability_P = [C_P; C_P*A; C_P*A^2; C_P*A^3]
rank(observability_P)

observability_theta = [C_theta; C_theta*A; C_theta*A^2; C_theta*A^3]
rank(observability_theta)
