pkg load control
clear;close all;clc
A=  [
    -0.003,  0.039,  0     , -0.322;
    -0.065, -0.319,  7.747 ,  0    ;
     0.020, -0.101, -0.429 ,  0    ;
     0    ,  0    ,  1     ,  0    ;
];
B = [
     0.01,  1.00;
    -0.18, -0.04;
    -1.16,  0.60;
     0   ,  0   ;
];

C = eye(4);
D = zeros(4,2);


controllability_delta_e = ctrb(A,B(:,1))
rank(controllability_delta_e)
controllability_delta_t = ctrb(A,B(:,2))
rank(controllability_delta_t)
controllability_delta_e_and_t = ctrb(A,B(:,1:2))
rank(controllability_delta_e_and_t)

observability_delta_theta = obsv(A,C(4,:))
rank(observability_delta_theta)
observability_delta_w_and_q = obsv(A,C(2:3,:))
rank(observability_delta_w_and_q)

P=lyap(A,eye(4))
eig(P)
