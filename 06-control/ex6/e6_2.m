cvx_solver sedumi

A = [-0.003, 0.039, 0, -0.322;
    -0.065, -0.319, 7.747, 0;
    0.02, -0.101, -0.429, 0;
    0, 0, 1, 0]

B = [0.01, 1.00; -0.18, -0.04; -1.16, 0.6; 0, 0]

C = [0, 1, 0, 0; 0, 0, 1, 0]

n = 4
cvx_begin sdp
    variable D(n,n) diagonal
    A'*D + D*A <= -eye(n)
    D >= eye(n)
cvx_end
D

cvx_begin sdp
    variable P(n,n) symmetric
    minimize(norm(P, 1))
    A'*P + P*A <= -eye(n)
    P >= eye(n)
cvx_end
P