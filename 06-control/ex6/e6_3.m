A = [-0.003, 0.039, 0, -0.322;
    -0.065, -0.319, 7.747, 0;
    0.02, -0.101, -0.429, 0;
    0, 0, 1, 0]
B = [0.01, 1.00; -0.18, -0.04; -1.16, 0.6; 0, 0]

cvx_begin sdp
    variable Q(4, 4) symmetric
    variable s
    subject to
        s > 0
        Q >= eye(4)
        A*Q + Q*A' + s*B*B' <= -eye(4)  
cvx_end
 
K = -(s/2)*B'*inv(Q)
s

cvx_begin sdp
     
    variable s
    variable Q(4, 4) symmetric
    variable F(2,4) % F = KQ
    subject to
        Q >= eye(4)
        A*Q - B*F + Q*A'-F'*B' <= -eye(4)
cvx_end


K = F*inv(Q)