function F = computeFundamentalMatrixRobust(pts2d_small, pts2d_big, K)
% compute all combinations of 8 points from our points
combinations_num = 8;
combinations = nchoosek(1:size(pts2d_small,2), combinations_num);

max_err = 1e10;
for c = 1:size(combinations,1)
    % select one combination of 8 points
    points2d_sample = pts2d_small(:,combinations(c,:), :);
    
    % compute candidate fundamental matrix
    F_candidate = computeFundamentalMatrix(points2d_sample);
    
    % enforce essential matrix to have equal singular values (as it should 
    % if we didn't have noise in the data) and rebuild F_candidate
    E = K' * F_candidate * K;
    [U, S, V] = svd(E);
    S(2,2) = S(1,1);
    E = U * S * V';
    
    F_candidate = inv(K)' * E * inv(K);
    
    % calculate error and select best F
    cur_err = norm(diag(diag(pts2d_big(:,:,2)' * F_candidate * pts2d_big(:,:,1))));
    if cur_err < max_err
        F = F_candidate;
        max_err = cur_err;
    end
end