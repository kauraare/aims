function point = triangulatePoint(point_i1, point_i2, P1, P2)
% compute the matrix A
A = [...
    P1(3,:) * point_i1(1) - P1(1,:);
    P1(3,:) * point_i1(2) - P1(2,:);
    P2(3,:) * point_i2(1) - P2(1,:);
    P2(3,:) * point_i2(2) - P2(2,:)];

% normalise A
for p = 1:4
    A(p,:) = A(p,:)/norm(A(p,:));
end

% compute the 3D point
[~, ~, point] = svd(A);
point = point(:, end);

point = point / point(end);