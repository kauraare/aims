% similar to FUNDMATRIX from Peter Kovesi http://www.csse.uwa.edu.au/~pk

function F = computeFundamentalMatrix(points2d)
% normalise each set of points so that the origin
% is at centroid and mean distance from origin is sqrt(2).
% normalise2dpts also ensures the scale parameter is 1.
[points1, T1] = normalise2dpts(points2d(:,:,1));
[points2, T2] = normalise2dpts(points2d(:,:,2));

% build the constraint matrix

A = [
    points2(1,:).*points1(1,:), points2(1,:).*points1(2,:), points2(1,:);
    points2(2,:).*points1(1,:), points2(2,:).*points1(2,:), points2(2,:);
    points1(1,:)              , points1(2,:)              , 1           ;
	];

% extract the fundamental matrix from the smallest singular value
[~,~,V] = svd(A,0);
F = reshape(V(:,9),3,3)';

% enforce constraint that fundamental matrix has rank 2 by performing
% a svd and then reconstructing with the two largest singular values.
[U, D, V] = svd(F,0);
F = U * diag([D(1,1) D(2,2) 0]) * V';

% denormalise
F = T2'*F*T1;