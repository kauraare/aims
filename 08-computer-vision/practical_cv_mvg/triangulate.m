function result = triangulate(points_i1, points_i2, P1, P2)
result = zeros(4, size(points_i1, 2));
for i=1:size(points_i1,2)
    % select the point from each image
    point_i1 = points_i1(:,i);
    point_i2 = points_i2(:,i);
    
    % triangulate 3D point
    result(:,i) = triangulatePoint(point_i1, point_i2, P1, P2);
end

% homogeneous coordinates
result = result./repmat(result(4,:),4,1);