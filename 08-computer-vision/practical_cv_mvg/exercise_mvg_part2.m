% Setup MATLAB to use our software
close all; clear;clc 
setup;

% Load images b and c
ia = imread('keble_a.jpg');
ib = imread('keble_b.jpg');
ic = imread('keble_c.jpg');

% Compute SIFT features for each
[frames_a, descrs_a] = getFeatures(ia, 'peakThreshold', 0.01) ;
[frames_b, descrs_b] = getFeatures(ib, 'peakThreshold', 0.01) ;
[frames_c, descrs_c] = getFeatures(ic, 'peakThreshold', 0.01) ;

% Plot frames
figure(1); clf;
subplot(1,3,1) ; imagesc(ia) ; axis equal off ; hold on ;
vl_plotframe(frames_a, 'linewidth', 2) ;
subplot(1,3,2) ; imagesc(ib) ; axis equal off ; hold on ;
vl_plotframe(frames_b, 'linewidth', 2) ;
subplot(1,3,3) ; imagesc(ic) ; axis equal off ; hold on ;
vl_plotframe(frames_c, 'linewidth', 2) ;






% Find for each descriptor in im1 the closest descriptor in im2
nn = findNeighbours(descrs_b, descrs_c);

% Construct a matrix of matches. Each column stores two index of
% matching features in ib and ic
matches = [1:size(descrs_b,2) ; nn(1,:)] ;

% Display the matches
figure(2); clf;
plotMatches(ib, ic, frames_b, frames_c, matches);

% Refine matches using geometric verification and compute homography
[inliers, Hcb] = geometricVerification(frames_b, frames_c, matches, 'numRefinementIterations', 8);







% Find for each descriptor in im1 the closest descriptor in im2
nn = findNeighbours(descrs_b, descrs_a);

% Construct a matrix of matches. Each column stores two index of
% matching features in ib and ia
matches = [1:size(descrs_b,2) ; nn(1,:)] ;

% Display the matches
figure(3); clf;
plotMatches(ib, ia, frames_b, frames_a, matches);

% Refine matches using geometric verification and compute homography
[inliers, Hab] = geometricVerification(frames_b, frames_a, matches, 'numRefinementIterations', 8);





% View result
figure(4);
bbox = [-400 1200 -200 700]; % image space for mosaic
iwb = vgg_warp_H(ib, eye(3), 'linear', bbox);  % warp image b to mosaic image
imshow(iwb); axis image;
iwc = vgg_warp_H(ic, Hcb, 'linear', bbox);  % warp image c to mosaic image
imshow(iwc); axis image;
iwa = vgg_warp_H(ia, Hab, 'linear', bbox);  % warp image c to mosaic image
imshow(iwa); axis image;
imagesc(double(max(iwa,max(iwb,iwc)))/255); % combine images into a common mosaic
axis image;
