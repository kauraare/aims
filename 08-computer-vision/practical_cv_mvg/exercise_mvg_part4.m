% load the two images
ima = imread('daliborka_01.jpg');
imb = imread('daliborka_23.jpg');

% load corresponding points in the two images
load('daliborka_points.mat');

% load the calibration matrix
load('daliborka_K.mat');

% compute fundamental matrix using robust built-in matlab function
F = estimateFundamentalMatrix(pts2d_big(1:2,:,1)', pts2d_big(1:2,:,2)', 'NumTrials', 2000);

%compute essential matrix from fundamental matrix and calibration
%enforce essential matrix to have equal singular values (as it should 
%if we didn't have noise in the data) and rebuild F_candidate
E = K' * F * K;
[U, S, V] = svd(E);
S(2,2) = S(1,1);
E = U * S * V';

%compute P1 and P2

%final triangulation for all points

figure(2); clf; clear pts2d_result;
labels = arrayfun(@num2str, 1:length(pts2d_big), 'UniformOutput', false);

% project to image 1
pts2d_result(:,:,1) = P1 * pts3d_result;
pts2d_result(:,:,1) = pts2d_result(:,:,1) ./ repmat(pts2d_result(3,:,1), 3, 1);
subplot(1,2,1);
image(ima); axis image; hold on;
draw_wire(pts2d_result(:,:,1), edges, 'y');

% project to image 2
pts2d_result(:,:,2) = P2 * pts3d_result;
pts2d_result(:,:,2) = pts2d_result(:,:,2) ./ repmat(pts2d_result(3,:,2), 3, 1);
subplot(1,2,2);
image(imb); axis image; hold on;
draw_wire(pts2d_result(:,:,2), edges, 'y');