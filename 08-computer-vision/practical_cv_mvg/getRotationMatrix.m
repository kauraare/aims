function R = getRotationMatrix(angles)
R = zeros(3,3);
cos_ang = cos(angles);
sin_ang = sin(angles);

R(1,1) = cos_ang(2) * cos_ang(3);
R(1,2) = sin_ang(1) * sin_ang(2) * cos_ang(3) + cos_ang(1) * sin_ang(3);
R(1,3) = -cos_ang(1) * sin_ang(2) * cos_ang(3) + sin_ang(1) * sin_ang(3);
R(2,1) = -cos_ang(2) * sin_ang(3);
R(2,2) = -sin_ang(1) * sin_ang(2) * sin_ang(3) + cos_ang(1) * cos_ang(3);
R(2,3) = cos_ang(1) * sin_ang(2) * sin_ang(3) + sin_ang(1) * cos_ang(3);
R(3,1) = sin_ang(2);
R(3,2) = -sin_ang(1) * cos_ang(2);
R(3,3) = cos_ang(1) * cos_ang(2);