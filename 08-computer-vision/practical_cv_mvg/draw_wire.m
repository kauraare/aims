function draw_wire( pts, edges, color )

sp = pts(:,edges(1,:));
ep = pts(:,edges(2,:));

if size(pts,1) == 2
  plot([sp(1,:); ep(1,:)], [sp(2,:); ep(2,:)],'color',color, 'LineWidth', 2);
elseif size(pts,1) == 3
  plot3([sp(1,:); ep(1,:)], [sp(2,:); ep(2,:)], [sp(3,:); ep(3,:)],'color',color, 'LineWidth', 2);
end


end

