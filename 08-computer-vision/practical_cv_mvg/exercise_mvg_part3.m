generate_synthetic;

points2d = points2d_synthetic;
K1 = calib_synthetic(:,:,1);
K2 = calib_synthetic(:,:,2);

%compute fundamental matrix from points
F = computeFundamentalMatrix(points2d);

%compute essential matrix from fundamental matrix and calibration
E = K2' * F * K1;

%compute P1 and P2
[P1, P2] = computeP1andP2(E, K1, K2, squeeze(points2d(:,1,:)));

%final triangulation for all points
points3d_result = triangulate(points2d_synthetic(:,:,1), points2d_synthetic(:,:,2), P1, P2);

% project to image 1
points2d_result(:,:,1) = P1 * points3d_result;
points2d_result(:,:,1) = points2d_result(:,:,1) ./ repmat(points2d_result(3,:,1), 3, 1);

% project to image 2
points2d_result(:,:,2) = P2 * points3d_result;
points2d_result(:,:,2) = points2d_result(:,:,2) ./ repmat(points2d_result(3,:,2), 3, 1);

% plot result and ground truth
figure;
plot3(points3d_synthetic(1,:), points3d_synthetic(2,:), points3d_synthetic(3,:), 'x')
figure;
plot3(points3d_result(1,:), points3d_result(2,:), points3d_result(3,:), 'x')

%plot image 1 original and with our reconstruction
figure;
plot(points2d_synthetic(1, :, 1), points2d_synthetic(2, :, 1), 'r');
figure;
plot(points2d_result(1, :, 1), points2d_result(2, :, 1), 'r')

%plot image 2 original and with our reconstruction
figure;
plot(points2d_synthetic(1, :, 2), points2d_synthetic(2, :, 2), 'r');
figure;
plot(points2d_result(1, :, 2), points2d_result(2, :, 2), 'r')