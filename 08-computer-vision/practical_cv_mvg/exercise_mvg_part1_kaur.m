close all % close all figures
clear all % clear variables

for index=1:11
    i(:,:,:,index)=imread(sprintf('img%03d.tif',index-1));
    figure(index);imshow(i(:,:,:,index));hold on
end


% get points
pairs=[
    6,7;
    7,8;
    8,9;
    9,10;
    10,11;
    5,6;
    4,5;
    3,4;
    2,3;
    1,2;
];


figure(11);
bbox=[-400 1200 -200 700]   % image space for mosaic

for ii=1:length(pairs,2)
    figure(pairs(ii,1)); [xb,yb] = ginput
    plot(xb,yb,'r+','markersize',7)  % marks selected points with a red cross

    figure(pairs(ii,2)); [xc,yc] = ginput
    plot(xc,yc,'r+','markersize',7)

Hbc = vgg_H_from_x_lin([xb, yb]', [xc,yc]')
vgg_gui_H(ib,ic,Hbc)


iwb = vgg_warp_H(ib, eye(3), 'linear', bbox);  % warp image b to mosaic image
imshow(iwb); axis image;

iwc = vgg_warp_H(ic, inv(Hbc), 'linear', bbox);  % warp image c to mosaic image
imshow(iwc); axis image;
imagesc(double(max(iwb,iwc))/255); % combine images into a common mosaic
axis image;



iwa = vgg_warp_H(ia, Hab, 'linear', bbox);
figure(4);

imagesc(double(max(iw,[],4))/255);
axis image;
