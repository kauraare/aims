function [P1, P2] = computeP1andP2(E, K1, K2, point)
% compute P1
P1 = K1 * [eye(3) zeros(3, 1)];

% compute t and the two Rs from E
[U, ~, V] = svd(E);
W = [0 -1 0; 1 0 0; 0 0 1];
t = U(:,end);
R1 = U * W * V'; R2 = U * W' * V';

% compute the 4 possible P2s
P2c(:,:,1) = K2 * [R1 t];
P2c(:,:,2) = K2 * [R2 t];
P2c(:,:,3) = K2 * [R1 -t];
P2c(:,:,4) = K2 * [R2 -t];

% select only one P2 using visibility test
point_3D = zeros(4,4); depth = zeros(4,2);
for i=1:4
    % select the point from each image
    point_i1 = point(:,1);
    point_i2 = point(:,2);

    % triangulate 3D point
    point_3D(:,i) = triangulatePoint(point_i1, point_i2, P1, P2c(:,:,i));
    
    % depth on first camera
    point_camera = P1(:,:) * point_3D(:,i);
    depth(i,1) = point_camera(3) / point_3D(end,i);
    
    % depth on second camera
    point_camera = P2c(:,:,i) * point_3D(:,i);
    depth(i,2) = point_camera(3) / point_3D(end,i);
end

% select correct P2 (for depths in both cameras > 0)
P2 = P2c(:, :, depth(:,1) > 0 & depth(:,2) > 0);