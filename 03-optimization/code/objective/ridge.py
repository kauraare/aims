import torch

from objective.base import Objective
from utils import assert_true


class Ridge(Objective):
    def _validate_inputs(self, w, x, y):
        assert_true(w.dim() == 2,
                    "Input w should be 2D")
        assert_true(w.size(1) == 1,
                    "Ridge regression can only perform regression (size 1 output)")
        assert_true(x.dim() == 2,
                    "Input datapoint should be 2D")
        assert_true(y.dim() == 1,
                    "Input label should be 1D")
        assert_true(x.size(0) == y.size(0),
                    "Input datapoint and label should contain the same number of samples")

class Ridge_ClosedForm(Ridge):
    def task_error(self, w, x, y):
        self._validate_inputs(w, x, y)
        error = torch.norm(torch.squeeze(x @ w) - y)**2/y.size(0)
        return error

    def oracle(self, w, x, y):

        self._validate_inputs(w, x, y)
        reg = self.hparams.mu/2 * torch.norm(w)**2
        error = self.task_error(w, x, y)
        obj = error + reg

        sol = torch.sum(2 * (torch.squeeze(x@w)-y) * torch.transpose(x,0,1), 1)
        print(sol)
        print(obj)
        return {'obj': obj, 'sol': sol}


class Ridge_Gradient(Ridge):
    def task_error(self, w, x, y):
        self._validate_inputs(w, x, y)
        # TODO: Compute mean squared error
        error = None
        return error

    def oracle(self, w, x, y):
        self._validate_inputs(w, x, y)
        # TODO: Compute objective value
        obj = None
        # TODO: compute gradient
        dw = None
        return {'obj': obj, 'dw': dw}
